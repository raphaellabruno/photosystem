<!DOCTYPE HTML>

<html>

<head>
	<title>Bruno Sales</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="assets/css/loginmain.css" />
	<link rel="stylesheet" href="assets/css/bootstraplogin.css" />
	<script src="assets/js/jquery-3.4.1.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/tooltip.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<noscript>
		<link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
</head>

<body class="is-preload">

	<!-- Page Wrapper -->
	<div id="page-wrapper">
	
	<?php  
	include('menu.html');           
	?>	

		<div class="container mt-3">
			<form class="text-center border border-light p-5" action="/controller/usuario_controller_login.php"
				method="POST" target="_self">
				<p class="h4 mb-4">Entrar</p>

				<input type="text" name="username" class="form-control mb-4" required placeholder="Nome de usuário">
				<input type="password" name="password" class="form-control mb-4" required placeholder="Senha">

				<button class="btn btn-info btn-block my-4" type="submit">Entrar</button>

				<p>Não tem conta?
					<a href="usuario_cadastro.html">Cadastrar</a>
				</p>
			</form>
		</div>


	</div>

	<!-- Scripts -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/jquery.scrollex.min.js"></script>
	<script src="assets/js/jquery.scrolly.min.js"></script>
	<script src="assets/js/browser.min.js"></script>
	<script src="assets/js/breakpoints.min.js"></script>
	<script src="assets/js/util.js"></script>
	<script src="assets/js/main.js"></script>

</body>

<?php  
include('footer.html');           
?>

</html>

