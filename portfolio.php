<!DOCTYPE HTML>
<!--
	Spectral by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Bruno Sales</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
	</head>
	<body class="is-preload">

		<!-- Page Wrapper -->
			<div id="page-wrapper">

				<!-- Header -->
					<header id="header">
						<h1><a href="index.html">BRUNO SALES</a></h1>
						<nav id="nav">
							<ul>
								<li class="special">
									<a href="#menu" class="menuToggle"><span>Menu</span></a>
									<div id="menu">
										<ul>
											<li><a href="index.html">Home</a></li>
											<li><a href="contact.html">Agendar</a></li>
											<li><a href="portfolio.html">Portfolio</a></li>
											<li><a href="about.html">Sobre</a></li>
											<li><a href="login.html">Log In</a></li>
										</ul>
									</div>
								</li>
							</ul>
						</nav>
					</header>

				<!-- Main -->
					<article id="main">
						<header>
							<h2>Portfólio</h2>
							<p>Conheça alguns dos meus trabalhos</p>
						</header>
						<section class="wrapper style5">
							<div class="inner">

								<section>
									<h4>Casal</h4>
									<h5>Ensaios em Casal</h5>
									<div class="box alt">
										<div class="row gtr-50 gtr-uniform">
											<div class="col-4"><span class="image fit"><img src="images/pic04.jpeg" alt="" /></span></div>
											<div class="col-4"><span class="image fit"><img src="images/pic06.jpeg" alt="" /></span></div>
											<div class="col-4"><span class="image fit"><img src="images/pic08.jpeg" alt="" /></span></div>
											<div class="col-5"><span class="image fit"><img src="images/pic02.jpg" alt="" /></span></div>
										</div>
									</div>
									</section>
									
								<section>
									<h4>Familia</h4>
									<h5>Ensaios em família</h5>
									<div class="box alt">
										<div class="row gtr-50 gtr-uniform">
											<!-- <div class="col-12"><span class="image fit"><img src="images/banner.jpg" alt="" /></span></div> -->
											
											<div class="col-6"><span class="image fit"><img src="images/pic05.jpeg" alt="" /></span></div>
											<div class="col-6"><span class="image fit"><img src="images/pic07.jpeg" alt="" /></span></div>
											<div class="col-4"><span class="image fit"><img src="images/pic12.jpg" alt="" /></span></div>
											<div class="col-5"><span class="image fit"><img src="images/pic09.jpeg" alt="" /></span></div>
											
										</div>
									</div>
									</section>
								<section>
									<h4>Individual</h4>
									<h5>Ensaios individuais</h5>
									<div class="box alt">
										<div class="row gtr-50 gtr-uniform">
											<div class="col-4"><span class="image fit"><img src="images/pic14.jpg" alt="" /></span></div>
											<div class="col-4"><span class="image fit"><img src="images/pic13.jpeg" alt="" /></span></div>
											<div class="col-4"><span class="image fit"><img src="images/pic10.jpeg" alt="" /></span></div>
											<div class="col-4"><span class="image fit"><img src="images/pic17.jpg" alt="" /></span></div>
											<div class="col-4"><span class="image fit"><img src="images/pic11.jpeg" alt="" /></span></div>
											<div class="col-4"><span class="image fit"><img src="images/pic15.jpg" alt="" /></span></div>
											<div class="col-4"><span class="image fit"><img src="images/pic16.jpg" alt="" /></span></div>
											
											
										</div>
									</div>
									</section>
									
								<section>
									<h4>Eventos</h4>
									<h5>Eventos em geral</h5>
									<div class="box alt">
										<div class="row gtr-50 gtr-uniform">
											<!-- <div class="col-12"><span class="image fit"><img src="images/banner.jpg" alt="" /></span></div> -->
											<div class="col-4"><span class="image fit"><img src="images/banner.jpg" alt="" /></span></div>
											<div class="col-4"><span class="image fit"><img src="images/pic03.jpg" alt="" /></span></div>
											<div class="col-4"><span class="image fit"><img src="images/pic18.jpg" alt="" /></span></div>

										</div>
									</div>
									</section>	
									

							</div>
						</section>
					</article>

				<!-- Footer -->
					<footer id="footer">
						<ul class="icons">
							<li><a href="#" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
							<li><a href="#" class="icon brands fa-dribbble"><span class="label">Dribbble</span></a></li>
							<li><a href="#" class="icon solid fa-envelope"><span class="label">Email</span></a></li>
						</ul>
						<ul class="copyright">
							<li>&copy; Untitled</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
						</ul>
					</footer>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>